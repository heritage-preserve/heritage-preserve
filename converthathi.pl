#!/usr/bin/perl

use Modern::Perl;

use Catmandu;
use Search::Elasticsearch;
use Catmandu::Importer::CSV;
use MARC::Record;

my $importer = Catmandu::Importer::CSV->new(
    file   => './hathi_full_20140401.txt',
    fields => [
        'identifier',      'access',
        'rights',          'hathinumber',
        'enum',            'source',
        'sourcenumber',    'oclc',
        'isbn',            'issn',
        'lccn',            'title',
        'imprint',         'rightsreason',
        'updatedate',      'govtdoc',
        'publicationdate', 'publicationplace',
        'language',        'format'
    ],
    sep_char => "\t"
);

my $e = Search::Elasticsearch->new();
my $n = $importer->each(
    sub {
        my $hashref = shift;
        my $results = $e->search(
            index => 'pubsnzmetadata',
            body =>
              { query => { match => { identifier => "(OCoLC)" . $hashref->{oclc} } } }
        );
warn "(OCoLC)".$hashref->{oclc};
        my $marc =
          json2marc( $results->{hits}->{hits}->[0]->{_source}->{record} );
        my $field =
          MARC::Field->new( '856', '', '',
            'u' => 'http://hdl.handle.net/2027/' . $hashref->{identifier} );
        $marc->append_fields($field);
        print $marc->as_usmarc;
    }
);

sub json2marc {
    my ($marcjson) = @_;

    my $marc = MARC::Record->new();
    $marc->encoding('UTF-8');

    # fields are like:
    # [ '245', '1', '2', 'a' => 'Title', 'b' => 'Subtitle' ]
    # conveniently, this is the form that MARC::Field->new() likes
    foreach my $field (@$marcjson) {
        next if @$field < 5;    # Shouldn't be possible, but...
        if ( $field->[0] eq 'LDR' ) {
            $marc->leader( $field->[4] );
        }
        else {
            my $marc_field = MARC::Field->new(@$field);
            $marc->append_fields($marc_field);
        }
    }
    return $marc;
}
